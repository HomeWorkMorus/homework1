package main.java.Homewthree;

import main.java.Homewthree.AnotationPack.FigureAnotation;
import main.java.Homewthree.AnotationPack.Perimetr;
import main.java.Homewthree.AnotationPack.Squar;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;

public class AnotRun {
    // private static final String PACK = Figure.class.getPackage().toString().split(" ")[1];
    private static final String SQUARE = "main.java.Homewthree.Square";
    private static final String TRIANGLE = "main.java.Homewthree.Triangle";
    private static final String RECTANGLE = "main.java.Homewthree.Rectangle";
    private static final String CIRCLE = "main.java.Homewthree.Circle";
    public static Class<?> aClass;
    public static Class<?> subCl;
    public static String pathClass;
    static Scanner scanner = new Scanner(System.in);
    ;

    public static void main(String[] args) throws ClassNotFoundException,
            InvocationTargetException, IllegalAccessException {
        variables();
    }


    static void variables() throws ClassNotFoundException, InvocationTargetException, IllegalAccessException {
        System.out.println("Добро пожаловать в программу!!! \nПО создано для вычисления Периметра и Площади фигур, " +
                "все фигуры и их методы вызываются при помощи рефликсии и имена фигур при помощи аннотаций.");
        while (true) {
            /**
             * Вызов метода figHelp() формирующий меню
             */
            figHelp();
            String num = scanner.next();
            switch (num) {
                case ("1"): {
                    Class<?> aClassq = aClass;
                    aClassq = Class.forName(SQUARE);
                    subCl = aClassq;
                    pathClass = SQUARE;
                    nameAnClass();
                    try {
                        /**
                         * Вызов метода run() в котором выполняется поиск аннотаций в классах фигур
                         * и формирования меню действий с фигурами подробное описания метода у самого метода
                         */
                        run();
                    } catch (InstantiationException | NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                }
                break;
                case ("2"): {
                    Class<?> aClassq = aClass;
                    aClassq = Class.forName(TRIANGLE);
                    subCl = aClassq;
                    pathClass = TRIANGLE;
                    nameAnClass();
                    try {
                        run();
                    } catch (InstantiationException | NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                }
                break;
                case ("3"): {
                    Class<?> aClassq = aClass;
                    aClassq = Class.forName(RECTANGLE);
                    subCl = aClassq;
                    pathClass = RECTANGLE;
                    nameAnClass();
                    try {
                        run();
                    } catch (InstantiationException | NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                }

                break;
                case ("4"): {
                    Class<?> aClassq = aClass;
                    aClassq = Class.forName(CIRCLE);
                    subCl = aClassq;
                    pathClass = CIRCLE;
                    nameAnClass();
                    try {
                        run();
                    } catch (InstantiationException | NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                }
                break;
                case ("5"):
                    System.exit(0);
                    break;
                default:
                    System.out.println("Введите корректные значения");

            }

        }
    }

    static void nameAnClass() {
        try {
            String clName = Class.forName(pathClass).getDeclaredAnnotation(FigureAnotation.class).nameFigure();
            System.out.println("Вы выбрали " + clName + " выбирете действие ");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     *
     * Метод run() выполняет поиск по классам фигур методов с аннотацией @Perimetr @Squar
     * и в случае нахождения в классах данных метододов с аннотацией записываает их параметры в переменные perim , sq
     * далее выводим в консоль (полученные аннотации) в анотациях объявленны дефолтные значения def()
     * по которым происходит выбор действия с фигурой значения 1 для периметра или 2 для площади
     * если введено значение "1" (Perimetr.class).def() perim вызывает значение коллеции subCl переданного в кейсе
     * если введено значение "2" (Squar.class).def() sq вызывает значение коллеции subCl переданного в кейсе
     * метод вызывается в кейсах метода variables()
     */
    static void run() throws IllegalAccessException, InvocationTargetException,
            InstantiationException, ClassNotFoundException, NoSuchMethodException {
        Method perim = null, sq = null;
        for (Method method : subCl.getDeclaredMethods()) {
            if (method.isAnnotationPresent(Perimetr.class)) {
                perim = method;
            }
            if (method.isAnnotationPresent(Squar.class)) {
                sq = method;
            }
        }
        assert perim != null;
        assert sq != null;
        String df;
        while (true) {
            System.out.println("Для вычисления " + perim.getDeclaredAnnotation(Perimetr.class).metName() + "(а) "
                    + sq.getDeclaredAnnotation(Squar.class).nameMeth() + "(и) ввести значения "
                    + perim.getDeclaredAnnotation(Perimetr.class).def() + " или "
                    + sq.getDeclaredAnnotation(Squar.class).def() + ", нажмите 3 для выхода в предыдущее меню.");
            df = scanner.next();
            if (df.equalsIgnoreCase(perim.getDeclaredAnnotation(Perimetr.class).def())) perim.invoke(subCl);
            if (df.equalsIgnoreCase(sq.getDeclaredAnnotation(Squar.class).def())) sq.invoke(subCl);
            if (df.equals("3")) break;
        }
    }

    /**
     * Метод figHelp() формирует меню программы при помощи аннотаций заданных
     * в классах фигур и выводит имя фигуры в консоль вызывается в методе variables()
     */
    static void figHelp() {
        try {
            System.out.println
                    ("\nВыбирете одну из фигур: \n 1. "
                            + Class.forName(SQUARE).getDeclaredAnnotation(FigureAnotation.class).nameFigure() + "\n 2. "
                            + Class.forName(TRIANGLE).getDeclaredAnnotation(FigureAnotation.class).nameFigure() + "\n 3. "
                            + Class.forName(RECTANGLE).getDeclaredAnnotation(FigureAnotation.class).nameFigure() + "\n 4. "
                            + Class.forName(CIRCLE).getDeclaredAnnotation(FigureAnotation.class).nameFigure()
                            + "\n 5. Выход");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
