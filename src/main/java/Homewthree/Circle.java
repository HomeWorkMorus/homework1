package main.java.Homewthree;

import main.java.Homewthree.AnotationPack.FigureAnotation;
import main.java.Homewthree.AnotationPack.Perimetr;
import main.java.Homewthree.AnotationPack.Squar;

import java.util.Scanner;

//implements Figure наследование интерфейса
@FigureAnotation(nameFigure = "Круг")
public class Circle {

    @Perimetr(metName = "Периметр")
    public static void calcPer() {
        //Действия с фигурами
        Scanner sc = new Scanner(System.in);
        double Per;
        double pi = Math.PI;
        System.out.println();
        double r;
        /**
         * Проверка на ввод не double значений если введено буквенное значение или недопустимые символы,
         * то программа не упадет в ошибку а просто выдаст сообщение что было введено неверное число и
         * иинициализирует ввод заново, по аналогии и в других класах реализованно подобным образом
         */
        do {
                System.out.print("Введите радиус круга :");
                while (!sc.hasNextDouble()) {
                    System.out.println("Это не число");
                    System.out.print("Введите число :");
                    sc.next();
                }
                r = sc.nextDouble();
            }
            while (r <= 0);
            System.out.println();
        Per = 2 * pi * r;
        System.out.println("Периметр КРУГА = " + Per +" см \n");

    }

    @Squar(nameMeth = "Площадь")
    public static void calcS() {
        Scanner sc = new Scanner(System.in);
        Double S;
        double pi = Math.PI;
        double r;
        do {
            System.out.print("Введите радиус круга : ");
            while (!sc.hasNextDouble()) {
                System.out.println("Это не число");
                System.out.print("Введите число :");
                sc.next();
            }
            r = sc.nextDouble();
        }
        while (r <= 0);
        System.out.println();

        double squre = 2;
        S = pi * Math.pow(r, squre);
        System.out.println("Площадь КРУГА = "+ S +" см^2 \n");
    }
}
