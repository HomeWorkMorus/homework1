package main.java.Homewthree;

import main.java.Homewthree.AnotationPack.FigureAnotation;
import main.java.Homewthree.AnotationPack.Perimetr;
import main.java.Homewthree.AnotationPack.Squar;

import java.util.Scanner;
@FigureAnotation(nameFigure = "Прямоугольник")
public class Rectangle {

    @Perimetr(metName = "Периметр")
    public static void calcPer() {
        Scanner sc = new Scanner(System.in);
        double Per;
        double side1;
        double side2;
        do {
            System.out.print("Введите сторону А : ");
            while (!sc.hasNextDouble()) {
                System.out.println("Это не число");
                System.out.print("Введите число :");
                sc.next();
            }
            side1 = sc.nextDouble();
        }
        while (side1 <= 0);
        System.out.println();



        do {
            System.out.print("Введите сторону В : ");
            while (!sc.hasNextDouble()) {
                System.out.println("Это не число");
                System.out.print("Введите число :");
                sc.next();
            }
            side2 = sc.nextDouble();
        }
        while (side2 <= 0);
        System.out.println();


        Per = 2 * (side1 + side2);
        System.out.println("Периметр ПРЯМОУГОЛЬНИКА = " + Per +" см\n");
    }

    @Squar(nameMeth = "Площадь")
    public static void calcS() {
        Scanner sc = new Scanner(System.in);
        double S;
        double side1;
        double side2;
        do {
            System.out.print("Введите сторону А : ");
            while (!sc.hasNextDouble()) {
                System.out.println("Это не число");
                System.out.print("Введите число :");
                sc.next();
            }
            side1 = sc.nextDouble();
        }
        while (side1 <= 0);
        System.out.println();



        do {
            System.out.print("Введите сторону В : ");
            while (!sc.hasNextDouble()) {
                System.out.println("Это не число");
                System.out.print("Введите число :");
                sc.next();
            }
            side2 = sc.nextDouble();
        }
        while (side2 <= 0);
        System.out.println();

        S = side1 * side2;
        System.out.println("Площадь ПРЯМОУГОЛЬНИКА = "+ S +" см^2 \n");

    }
}
