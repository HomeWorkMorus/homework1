package main.java.Homewthree.AnotationPack;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Аннотация используется для метода calcPer() в классах фигур
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Perimetr {
    String metName ();

    /**
     * Значение по дефолту которое будет вызвано в методе run() при вводе в консоль "1" произойдет проверка вызова метод
     * @return
     */
    String def() default "1";
}
