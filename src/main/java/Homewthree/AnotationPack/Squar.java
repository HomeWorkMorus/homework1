package main.java.Homewthree.AnotationPack;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Аннотация используется для метода calcS() в классах фигур
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Squar {
    String nameMeth ();
    String def() default "2";
}
