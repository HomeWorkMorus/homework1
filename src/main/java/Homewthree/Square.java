package main.java.Homewthree;

import main.java.Homewthree.AnotationPack.FigureAnotation;
import main.java.Homewthree.AnotationPack.*;

import java.util.Scanner;

@FigureAnotation(nameFigure = "Квадрат")
public class Square  {

@Perimetr(metName = "Периметр")
    public static void calcPer() {
        Scanner sc = new Scanner(System.in);
        double Per;
        double side;

        do {
            System.out.print("Введите сторону А : ");
            while (!sc.hasNextDouble()) {
                System.out.println("Это не число");
                System.out.print("Введите число :");
                sc.next();
            }
            side = sc.nextDouble();
        }
        while (side <= 0);
        System.out.println();

        Per = 4 * side;
        System.out.println("Периметр КВАДРАТА = " + Per +" см \n");
    }


    @Squar(nameMeth = "Площадь")
    public static void calcS() {
        Scanner sc = new Scanner(System.in);
        double S;
        double side;

        do {
            System.out.print("Введите сторону А : ");
            while (!sc.hasNextDouble()) {
                System.out.println("Это не число");
                System.out.print("Введите число :");
                sc.next();
            }
            side = sc.nextDouble();
        }
        while (side <= 0);
        System.out.println();

        double squre = 2;
        S = Math.pow(side, squre);
        System.out.println("Площадь КВАДРАТА = "+ S +" см^2 \n");
    }
}
