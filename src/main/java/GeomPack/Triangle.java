package main.java.GeomPack;

import java.util.Scanner;

public class Triangle implements Figure {


    @Override
    public void calcPer() {
        Scanner sc = new Scanner(System.in);
        double Per;
        double sideA;
        double sideB;
        double sideC;
        String flag = "";
        String egg = "";
        do {
            System.out.print("Введите сторону A : ");
            while (!sc.hasNextDouble()) {
                System.out.println("Это не число");
                System.out.print("Введите число :");
                sc.next();
            }
            sideA = sc.nextDouble();
        }
        while (sideA <= 0);
        System.out.println();

        do {
            System.out.print("Введите сторону B : ");
            while (!sc.hasNextDouble()) {
                System.out.println("Это не число");
                System.out.print("Введите число :");
                sc.next();
            }
            sideB = sc.nextDouble();
        }
        while (sideB <= 0);
        System.out.println();


        do {
            System.out.print("Введите сторону C : ");
            while (!sc.hasNextDouble()) {
                System.out.println("Это не число");
                System.out.print("Введите число :");
                sc.next();
            }
            sideC = sc.nextDouble();
        }
        /**
         * Проверка на существования треугольника
         */
        while (sideC <= 0);
        System.out.println();

        if (sideA + sideB > sideC) {
            if (sideA + sideC > sideB) {
                if (sideB + sideC > sideA) {
                    Per = sideA + sideB + sideC;
                    System.out.println("Периметр ТРЕУГОЛЬНИКА = " + Per + " см \n");
                    if (Per == 12.0) {
                        egg += "You";
                        System.out.println(egg + " are beautiful\n");
                    }
                } else {
                    flag = "A";
                }
            } else {
                flag = "B";
            }
        } else {
            flag = "C";

        }
        /**
         * Проверка если переменная flag не пустая то производим запись в переменную side,
         * а переменную side выводи в консоль.
         */
        if (flag != "") {
            String side = flag;
            System.out.println("Треугольник с данными параметрами не существует");
            System.out.println("Сторона " + side + " большее 2 ух других сторон");
        }
    }

    @Override
    public void calcS() {
        Scanner sc = new Scanner(System.in);
        double S;
        double sideA;
        double h;

        do {
            System.out.println("Введите сторону A : ");
            while (!sc.hasNextDouble()) {
                System.out.println("Это не число");
                System.out.print("Введите число :");
                sc.next();
            }
            sideA = sc.nextDouble();
        }
        while (sideA <= 0);
        System.out.println();


        do {
            System.out.println("Введите высоту h : ");
            while (!sc.hasNextDouble()) {
                System.out.println("Это не число");
                System.out.print("Введите число :");
                sc.next();
            }
            h = sc.nextDouble();
        }
        while (h <= 0);
        System.out.println();

        S = (1 * (sideA * h)) / 2;
        System.out.println("Площадь ТРЕУГОЛЬНИКА = " + S + " см^2 \n");
    }
}
