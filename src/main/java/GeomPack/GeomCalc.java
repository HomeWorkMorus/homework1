package main.java.GeomPack;

import java.util.Scanner;

public class GeomCalc {
    public static void main(String[] args) {
        /**
         * Переменные описания меню
         */
        String actions = "Выберете действие \n 1. Периметр \n 2. Площадь \n 3. Возврат в предыдущее меню";
        String def2 = "Выберети значение 1, 2  или 3";
        /**
         * Переменная fig ссылкочная переменная ссылается на интерфейс Figure
         */
        Figure fig;
        System.out.println("Добро пожаловать ! данное ПО разработанно для вычисления Периметра и Площади разных фигур.");
        /**
         * Иницализация объекта сканера (System.in обозначает что будет осуществляться ввод символов)
         */
        Scanner scanner = new Scanner(System.in);
        /**
         * Бесконечный цикл для показа меню
         */
        while (true) {
            System.out.println("Выберети фигуру : \n 1. Треугольник \n 2. Круг \n 3. Квадрат \n 4. Прямоугольник \n 5. Выход");
            //Переменная figure служит для реализации ввода с клавиатуры
            String figure = scanner.nextLine();
            /**
             * После ввода символов происходит попадание в кейс в кейс можно попасть при вводе символов от 1 до 5
             */
            switch (figure) {
                case ("1"):
                    // обращащение к интерфейсу
                    fig = new Triangle();
                    System.out.println("\n ТРЕУГОЛЬНИК");
                    System.out.println(actions);
                    String act = scanner.nextLine();
                    /**
                     *  При попадании в кейс происходит запуск 2 го вложенного кейса отвечающего за действия с фигурами по аналогии реализованы и другие кейсы
                     */

                    switch (act) {
                        case ("1"):
                            fig.calcPer();
                            break;
                        case ("2"):
                            fig.calcS();
                            break;
                        case ("3"):
                            /**
                             *  Выход в предыдущее меню
                              */

                            break;
                        default:
                            /**
                             * При выборе неверных значений будет возвращен default
                             */
                            System.out.println(def2);
                    }
                    break;
                case ("2"):
                    fig = new Circle();
                    System.out.println("\n КРУГ");
                    System.out.println(actions);
                    String act2 = scanner.nextLine();
                    switch (act2) {
                        case ("1"):
                            fig.calcPer();
                            break;
                        case ("2"):
                            fig.calcS();
                            break;
                        case ("3"):
                            break;
                        default:
                            System.out.println(def2);
                    }
                    break;
                case ("3"):
                    fig = new Square();
                    System.out.println("\n КВАДРАТ");
                    System.out.println(actions);
                    String act3 = scanner.nextLine();
                    switch (act3) {
                        case ("1"):
                            fig.calcPer();
                            break;
                        case ("2"):
                            fig.calcS();
                            break;
                        case ("3"):
                            break;
                        default:
                            System.out.println(def2);
                    }
                    break;

                case ("4"):
                    fig = new Rectangle();
                    System.out.println("\n ПРЯМОУГОЛЬНИК");
                    System.out.println(actions);
                    String act4 = scanner.nextLine();
                    switch (act4) {
                        case ("1"):
                            fig.calcPer();
                            break;
                        case ("2"):
                            fig.calcS();
                            break;
                        case ("3"):
                            break;
                        default:
                            System.out.println(def2);
                    }
                    break;

                case ("5"):
                    /**
                     * Выход из программы
                     */

                    System.exit(0);
                    break;
                default:
                    System.out.println("Выберети значение от 1 до 5");

            }

        }
    }
}
