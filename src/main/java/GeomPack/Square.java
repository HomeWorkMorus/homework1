package main.java.GeomPack;

import java.util.Scanner;

public class Square implements Figure {
    @Override
    public void calcPer() {
        Scanner sc = new Scanner(System.in);
        double Per;
        double side;

        do {
            System.out.print("Введите сторону А : ");
            while (!sc.hasNextDouble()) {
                System.out.println("Это не число");
                System.out.print("Введите число :");
                sc.next();
            }
            side = sc.nextDouble();
        }
        while (side <= 0);
        System.out.println();

        Per = 4 * side;
        System.out.println("Периметр КВАДРАТА = " + Per +" см \n");
    }

    @Override
    public void calcS() {
        Scanner sc = new Scanner(System.in);
        double S;
        double side;

        do {
            System.out.print("Введите сторону А : ");
            while (!sc.hasNextDouble()) {
                System.out.println("Это не число");
                System.out.print("Введите число :");
                sc.next();
            }
            side = sc.nextDouble();
        }
        while (side <= 0);
        System.out.println();

        double squre = 2;
        S = Math.pow(side, squre);
        System.out.println("Площадь КВАДРАТА = "+ S +" см^2 \n");
    }
}
